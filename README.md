# ansible-roles

This repository contains all Ansible roles to set up a Debian 9 server for comfortable administration using Docker Compose.

## Example Playbook

```yaml
- name: Set up everything
  hosts: all
  roles:
    - role: essentials
      tags: [essentials]
      vars:
        permit_root_login: "no"
    - role: user
      tags: [essentials, user]
      vars:
        username: "..."
        password: "..." # mkpasswd --method=sha-512
        authorized_keys: |
          ssh-ed25519 ...
    - role: iptables
      tags: [essentials, iptables]
    - role: docker
      tags: [docker]
```

You can run this with the following commands:
```
# clone ansible-roles
git clone https://codeberg.org/momar/ansible-roles.git roles
# edit the hosts file
vi hosts
# run the playbook
ansible-playbook -i hosts <filename.yml> -Kbu <username-or-root>
```

## Roles

### essentials
Install packages for easier administration of the system, including fancy tools like [micro](https://github.com/zyedidia/micro) and [hexyl](https://github.com/sharkdp/hexyl), and a comfortable zshrc environment.

**WARNING: This disables password authentication for SSH, so you better make sure that you have a key set up.**

If you're also creating a user, use the following to disable root login via SSH:
```yaml
roles:
- role: essentials
    vars:
      permit_root_login: no
```


### user
Create a user with sudo rights, an SSH key and an authorized_keys file:
```yaml
roles:
- role: user
    vars:
      username: "..."
      password: "..." # mkpasswd --method=sha-512
      authorized_keys: |
        ssh-ed25519 ...
```

### iptables
Create a default iptables configuration and use `/data/firewall.rules` for additional service rules.

### docker
Install Docker and Docker Compose, and create a `/data` folder for all docker projects. You should use [Traefik](https://traefik.io) to easily expose web servers.

### data-backup
Backup `/data` with restic to an offsite location. Includes `db-backup`. Example:
```yaml
roles:
- role: data-backup
    vars:
      restic: |
        export RESTIC_REPOSITORY='b2:somewhere:{{ ansible_facts['nodename'] }}'
        export RESTIC_PASSWORD='...'
        export B2_ACCOUNT_ID='...'
        export B2_ACCOUNT_KEY='...'
```

# TODO:
- send logs to logstash
  - systemd
    - https://www.elastic.co/guide/en/logstash/6.5/plugins-inputs-syslog.html
    - https://raymii.org/s/tutorials/Syslog_config_for_remote_logservers_for_syslog-ng_and_rsyslog_client_server.html
  - docker
    - https://logz.io/blog/logz-io-docker-log-collector/
    - https://github.com/gliderlabs/logspout
    - https://github.com/looplab/logspout-logstash
- dynamic-dns
- raid
- gluster

# System-wide .bashrc file for interactive bash(1) shells.

# To enable the settings / commands in this file for login shells as well,
# this file has to be sourced in /etc/profile.

# If not running interactively, don't do anything
[ -z "$PS1" ] && return

# check the window size after each command and, if necessary,
# update the values of LINES and COLUMNS.
shopt -s checkwinsize

# set variable identifying the chroot you work in (used in the prompt below)
if [ -z "${debian_chroot:-}" ] && [ -r /etc/debian_chroot ]; then
    debian_chroot=$(cat /etc/debian_chroot)
fi

# set a fancy prompt
PS1='$(cs="\[" ce="\]" /opt/prompt/prompt)'

# enable bash completion in interactive shells
if ! shopt -oq posix; then
  if [ -f /usr/share/bash-completion/bash_completion ]; then
    . /usr/share/bash-completion/bash_completion
  elif [ -f /etc/bash_completion ]; then
    . /etc/bash_completion
  fi
fi

# if the command-not-found package is installed, use it
if [ -x /usr/lib/command-not-found -o -x /usr/share/command-not-found/command-not-found ]; then
	function command_not_found_handle {
	        # check because c-n-f could've been removed in the meantime
                if [ -x /usr/lib/command-not-found ]; then
		   /usr/lib/command-not-found -- "$1"
                   return $?
                elif [ -x /usr/share/command-not-found/command-not-found ]; then
		   /usr/share/command-not-found/command-not-found -- "$1"
                   return $?
		else
		   printf "%s: command not found\n" "$1" >&2
		   return 127
		fi
	}
fi

PATH="$HOME/bin:$HOME/.local/bin:$PATH"

# include the user's .bashrc, if existant
if [ -f "$HOME/.bashrc" ]; then
	. "$HOME/.bashrc"
fi

# set up aliases
alias ls="exa"
alias la="exa -a"
alias ll="exa --git -la"
alias l="ll"
alias lsl="ls"
alias sls="ls"
alias lsls="ls"
alias sl="ls"

alias qbin="curl https://beta.qbin.io -s -T"
transfer() { if [ $# -eq 0 ]; then echo -e "No arguments specified. Usage:\necho transfer /tmp/test.md\ncat /tmp/test.md | transfer test.md"; return 1; fi; tmpfile=$( mktemp -t transferXXX ); if tty -s; then basefile=$(basename "$1" | sed -e 's/[^a-zA-Z0-9._-]/-/g'); curl --progress-bar --upload-file "$1" "https://transfer.sh/$basefile" >> $tmpfile; else curl --progress-bar --upload-file "-" "https://transfer.sh/$1" >> $tmpfile ; fi; cat $tmpfile; rm -f $tmpfile; } 
alias pw='bash -c '"'"'echo `tr -dc $([ $# -gt 1 ] && echo $2 || echo "A-Za-z0-9") < /dev/urandom | head -c $([ $# -gt 0 ] && echo $1 || echo 30)`'"'"' --'
alias yq="python3 -c 'import sys, yaml, json; json.dump(yaml.load(sys.stdin), sys.stdout, indent=4)' | jq"

export EDITOR="/usr/local/bin/micro"

if [ `id -u` -eq 0 -a -z "$root_shell_notice_shown" ];
then . /root/.profile
fi

if [ `id -u` -eq 0 ]; then 
    start=`tput setab 1; tput setaf 7; tput bold`
    end=`tput setab 9; tput setaf 9; tput sgr0`
    echo
    echo "  $start                                                                       $end"
    echo "  $start  WARNING: You are in a root shell. This is probably a very bad idea.  $end"
    echo "  $start                                                                       $end"
    echo
fi

# print tip of the day
tput setaf 6 # cyan
echo -n "Tip of the day: "
shuf -n 1 /etc/tip-of-the-day.txt
tput setaf 9 # reset
